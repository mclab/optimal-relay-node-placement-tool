# Optimal Fault-Tolerant Placements of Relay Nodes in Mission Critical Wireless Networks

A decision-support tool that, by exploiting specialised algorithms that scale from commodity PCs to a HPC infrastructure, generates (from terrain data and placement constraints) a 0-1 LP formulation of the optimal relay node placement problem presented in [Optimal Fault-Tolerant Placements of Relay Nodes in Mission Critical Wireless Networks](pending), enabling the use of standard off-the-shelf AI-based reasoners like MILP, PB, as well as SMT/OMT solvers.

## Features

With this tool you can:
- Generate a visibility graph showing the allowed deployment positions of the relay nodes
- Generate 0-1 LP formulation of the cost-optimal relay node placement problem 
- Generate a budget restricted formulation of the relay node placement problem
- Export the problem formulation in:
  - The LP format, taken as input from most MILP solvers;
  - The OPB language, taken as input from most PB solvers (see [http://www.cril.univ-artois.fr/PB16]);
  - The SMT-Lib standard language, with extensions to express objective functions as supported by the Z3 and OptiMathSat SMT/OMT solvers.
- Solve the generated instances with severlar solvers:
  - [CPLEX](http://www-01.ibm.com/support/docview.wss?uid=swg27050618), [Gurobi](http://www.gurobi.com), [Glpk](http://www.gnu.org/software/glpk)
  - [Sat4j](http://sat4j.org), [NAPS](https://www.trs.cm.is.nagoya-u.ac.jp/NaPS/), [MiniSat+](http://minisat.se/MiniSat+.html)
  - [Z3](https://github.com/Z3Prover/z3), [OptiMathSat](http://optimathsat.disi.unitn.it/)

## 1. Quickstart
### 1.0 Dependency

- python3
- docker
- git (optional)

This tool uses [Docker](https://docs.docker.com/install/) to simplify the deployment process.

If you want to use the included configuration tool `conf.py` which provides a simpler interface to this tool, make sure to have [Python3](https://www.python.org/downloads/) installed

### 1.1 Download the container

Download the container from [Docker hub](https://hub.docker.com/)

```bash
docker pull mclab/optimal-relay-node-placement
```

### 1.2 Download the configuration software

Download the configuration software from git clone https://bitbucket.org/mclab/optimal-relay-node-placement-tool.git

Or clone this repository

```bash
git clone https://bitbucket.org/mclab/optimal-relay-node-placement-tool.git
```
### 1.3 Run the configuration software

```bash
python3 conf.py \
    --input /absolute/path/to/input \
    --output /absolute/path/to/output \
    --cplex /absolute/path/to/cplex/folder \
    --gurobi /absolute/path/to/gurobi_home/
```

### 1.4 Choose the benchmark scenario

Running the configuration tool you will be prompted to choose among four benchmark scenarios

## 2. Advanced Usage

You can also interface directly with the tool.

The entrypoint is `benchmark.py` in the docker container and can be accessed whith docker

```bash
docker run -it --rm \
    -v /absolute/path/to/input:/input
    -v /absolute/path/to/output:/output
    -v /absolute/path/to/cplex:/opt/cplex
    -v /absolute/path/to/gurobi:/opt/gurobi
    mclab/optimal-relay-node-placement-tool \
    python3 benchmark.py
```
### 2.1 Parameters

> WIP

## A. Contact us

> WIP