import subprocess
import argparse
import os
parser = argparse.ArgumentParser(description='Execute standard benchmarks')
parser.add_argument('--output', dest='output', help='save the results to [output] folder', required=True)
parser.add_argument('--input', dest='input', help='read scenario definitions from [input] folder', required=True)
parser.add_argument('--cplex', dest='cplex', help='path to cplex executable folder (e.g. /opt/ibm/ILOG/CPLEX_Studio128/cplex/bin/x86-64_linux/)', default='/opt/ibm/ILOG/CPLEX_Studio128/cplex/bin/x86-64_linux/')
parser.add_argument('--gurobi', dest='gurobi', help='path to gurobi installation folder (e.g. /opt/gurobi810/)', default='/opt/gurobi810/')
args = parser.parse_args()

if __name__ == '__main__':
    output_path = args.output
    input_path = args.input
    cplex_path = args.cplex
    gurobi_path = args.gurobi
    print('Which benchmark scenario do you want to test?')
    print('1. optimisation\t- test the complete set of instances as described in the paper as optimisation problems (make sure to have enough disk space and compute resources)')
    print('2. feasibility\t- test the complete set of instances as described in the paper as feasibility problems')
    print('3. fast-optimisation\t- test only a subset of the instances with a lower timelimit (60s) as optimisation problems')
    print('4. fast-feasibility\t- test only a subset of the instances with a lower timelimit (60s) as feasibility problems')
    test_case = int(input('> '))
    docker_args = [
        'docker',
        'run',
        '-it',
        '-u', str(os.getuid()),
        '-v', '{path}:/opt/cplex'.format(path=args.cplex),
        '-v', '{path}:/opt/gurobi'.format(path=args.gurobi),
        '-v', '{path}:/input'.format(path=args.input),
        '-v', '{path}:/output'.format(path=args.output),
        'relay-node-placement',
        'bash'
    ]

    if test_case == 1:
        p = subprocess.Popen(docker_args + ['benchmark-optimisation.sh'])
    if test_case == 2:
        p = subprocess.Popen(docker_args + ['benchmark-feasibility.sh'])
    if test_case == 3:
        p = subprocess.Popen(docker_args + ['benchmark-optimisation-fast.sh'])
    if test_case == 4:
        p = subprocess.Popen(docker_args + ['benchmark-feasibility-fast.sh'])
    p.wait()
